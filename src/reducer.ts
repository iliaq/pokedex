import {
  POKE_LIST_FETCHED,
  POKE_LIST_FAILED,
  ADD_TO_MY_LIST,
  RM_FROM_MY_LIST,
  POKE_DETAILS_FETCHED,
  POKE_DETAILS_FAILED,
} from './actions';
export const defaultState: State = {
  results: [],
  my: [],
  details: {},
};
export function reducer(state = defaultState, { type, payload }) {
  var newState = {},
    error = '';
  switch (type) {
    case POKE_LIST_FETCHED:
      newState = { ...payload, results: state.results.concat(payload.results) };
      break;
    case POKE_DETAILS_FETCHED:
      newState = { details: { [payload.id]: payload } };
      break;
    case POKE_LIST_FAILED:
      error = 'failed fetching poke list';
      break;
    case POKE_DETAILS_FAILED:
      error = 'failed fetching poke details';
      break;
    case ADD_TO_MY_LIST:
      newState = add(state, payload);
      break;
    case RM_FROM_MY_LIST:
      newState = rm(state, payload);
      break;
  }
  return { ...state, error, ...newState };
}

function add(state, payload) {
  const doesExists = state.my.find(({ url }) => url === payload.url);
  if (doesExists) {
    return {};
  }
  const newState = { my: state.my.concat(payload) };
  try {
    localStorage.setItem('my', JSON.stringify(newState.my));
  } catch (e) {}
  return newState;
}
function rm(state, payload) {
  const doesExists = state.my.find(({ url }) => url === payload);
  if (!doesExists) {
    return { error: 'cant remove pokemon from list' };
  }
  const index = state.my.indexOf(doesExists);
  const my = state.my.concat([]);
  my.splice(index, 1);
  const newState = { my };
  try {
    localStorage.setItem('my', JSON.stringify(newState.my));
  } catch (e) {}
  return newState;
}

export interface State {
  results: any[];
  my: any[];
  error?: string;
  details: any;
}
