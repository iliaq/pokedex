import React, { PureComponent } from 'react';
import Paper from 'material-ui/Paper';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Avatar';
import Table, { TableBody, TableCell, TableRow } from 'material-ui/Table';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import { BASE_URL, BASE_IMG } from '../api/middleware';
import { Checkbox } from 'material-ui';
export default class PokeDetails extends PureComponent<Props, {}> {
  render() {
    const {
      id,
      name,
      weight,
      height,
      is_default,
      moves,
      sprites,
      base_experience,
      addToMyList,
      removeFromMyList,
      my,
      // order,
      // types,
      // abilities,
      // stats,
      // species,
      // game_indices,
    } = this.props;
    const url = `${BASE_URL}/${id}/`;
    const Sprites =
      sprites &&
      Object.keys(sprites).map(key => (sprites[key] ? <img key={key} src={sprites[key]} alt={key} /> : null));
    //const Species = species && link(species);
    const add = () => addToMyList({ url, name });
    const rm = () => removeFromMyList(url);
    const isInMyList = my.length > 0 && my.find(fav => fav.name === name);
    const ava = [BASE_IMG, id, '.png'].join('');
    return (
      <Paper>
        <Typography variant="headline" component="h1" align="center">
          {name}
        </Typography>
        <Avatar src={ava} />
        {isInMyList ? (
          <Button variant="fab" color="secondary" aria-label="add" onClick={rm}>
            <DeleteIcon />
          </Button>
        ) : (
          <Button variant="fab" color="primary" aria-label="add" onClick={add}>
            <AddIcon />
          </Button>
        )}
        <Divider />
        {Sprites}

        <Typography variant="title" component="h2" align="center">
          Description
        </Typography>
        <Table>
          <TableBody>
            <TableRow key="weight">
              <TableCell>weight</TableCell>
              <TableCell>{weight}</TableCell>
            </TableRow>
            <TableRow key="height">
              <TableCell>height</TableCell>
              <TableCell>{height}</TableCell>
            </TableRow>
            <TableRow key="base_experience">
              <TableCell>base_experience</TableCell>
              <TableCell>{base_experience}</TableCell>
            </TableRow>
            <TableRow key="is_default">
              <TableCell>is default</TableCell>
              <TableCell>
                <Checkbox checked={is_default} disabled />
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
        <Divider />
        <Typography variant="title" component="h2" align="center">
          Moves
        </Typography>
        {moves ? (
          <Table>
            <TableBody>
              {moves.map(({ version_group_details, move }) => (
                <TableRow key={move.name}>
                  <TableCell>{link(move)}</TableCell>
                  <TableCell>
                    <details>
                      <summary>version group details</summary>
                      {version_group_details.map(({ move_learn_method, level_learned_at, version_group }) => (
                        <div key={`${level_learned_at}${move_learn_method.name}${version_group.name}`}>
                          level learned at: {level_learned_at} <br />
                          move learn method: {link(move_learn_method)} <br />
                          version group: {link(version_group)}
                        </div>
                      ))}
                    </details>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        ) : (
          <div>no moves :(</div>
        )}
      </Paper>
    );
  }
}

//description, stats, moves etc

function link({ url, name }) {
  return <a href={url}>{name}</a>;
}

export interface Props {
  order: number;
  name: string;
  weight: number;
  height: number;
  types: any[];
  is_default: boolean;
  abilities: any[];
  stats: any[];
  moves: any[];
  sprites: any;
  species: Link;
  base_experience: number;
  game_indices: any[];
  id: number;
  my: any[];
  addToMyList(a: any): any;
  removeFromMyList(a: any): any;
}
interface Link {
  name: string;
  url: string;
}

// <details>
//           {types &&
//             types.map(({ slot, type }) => (
//               <li key={slot}>
//                 slot: {slot}
//                 <br />
//                 {link(type)}
//               </li>
//             ))}
//         </details>

// <details>
// <summary>Abilities</summary>
// <ul>
//   {abilities.map(({ slot, is_hidden, ability }) => (
//     <li key={`${slot}${ability.name}`}>
//       <div>slot: {slot}</div>
//       <div>is hidden: {is_hidden}</div>
//       <div>ability: {link(ability)}</div>
//     </li>
//   ))}
// </ul>
// </details>
// <details>
// <summary>Stats</summary>
// <ul>
//   {stats.map(({ effort, base_stat, stat }) => (
//     <li key={stat.name}>
//       <div>effort: {effort}</div>
//       <div>base stat: {base_stat}</div>
//       <div>stat: {link(stat)}</div>
//     </li>
//   ))}
// </ul>
// </details>

// <details>
// <summary>Game indices</summary>
// <ul>
//   {game_indices.map(({ game_index, version }) => (
//     <li key={`${game_index}${version.name}`}>
//       game_index: {game_index}
//       <br />
//       version: {link(version)}
//     </li>
//   ))}
// </ul>
// </details>
