import React, { PureComponent } from 'react';
import TextField from 'material-ui/TextField';
export default class Search extends PureComponent<Props, {}> {
  render() {
    const { onChange } = this.props;
    return <TextField onChange={onChange} id="search" label="filter" margin="normal" />;
  }
}

export interface Props {
  onChange(e: any): any;
}
/* 
interface State {
  count: number;
  results: any;
}
 */
