import React, { PureComponent } from 'react';
import Button from 'material-ui/Button';
import { Link } from 'react-router-dom';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Icon from 'material-ui/Icon';
import Badge from 'material-ui/Badge';
export default class AppBarComponent extends PureComponent<Props, {}> {
  render() {
    const { count } = this.props;
    return (
      <AppBar position="static">
        <Toolbar>
          <Button component={Link} to="/">
            Pokedex
          </Button>
          <Button component={Link} to="/my">
            <Badge badgeContent={count} color="secondary">
              <Icon>star</Icon>
            </Badge>
          </Button>
        </Toolbar>
      </AppBar>
    );
  }
}

export interface Props {
  count: number;
}
