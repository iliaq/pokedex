import React, { PureComponent, Fragment } from 'react';
import { BASE_URL, BASE_IMG } from '../api/middleware';
import { CircularProgress } from 'material-ui/Progress';
import Button from 'material-ui/Button';
import { Link } from 'react-router-dom';
import Avatar from 'material-ui/Avatar';
import { AppBar } from '..';
export default class PokeList extends PureComponent<Props, {}> {
  render() {
    const { results, my } = this.props;
    return results.length > 0 ? results.map(li) : <div>list is empty</div>;
  }
}

function li({ url, name }) {
  const i = url.split('/')[6];
  const ava = [BASE_IMG, i, '.png'].join('');
  const href = url.replace(BASE_URL, '/details');
  return (
    <Button key={i} component={Link} to={href} size="small">
      <Avatar src={ava} />
      {name}
    </Button>
    // <li >
    //   <a href={href}>{name}</a>
    // </li>
  );
}

export interface Props {
  count: number;
  results: any;
  my: any[];
}
