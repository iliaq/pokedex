import { push } from 'react-router-redux';
const location = window.location || document.location;
const origin =
  location.origin || `${location.protocol}//${location.hostname}` + (location.port ? `:${location.port}` : '');

export default dispatch => event => {
  const a = event.path ? getAnchorByPath(event.path) : getAnchorByParent(event.target);
  if (!a) {
    return false;
  }
  const href = a.pathname || a.getAttribute('href');
  if (!href) {
    return false;
  }
  if ([isModifiedEvent, isNotLeftClickEvent, isPrevented].some(cb => cb(event))) {
    return true;
  }
  if (isNotValid(a, href) || isExternal(a, origin)) {
    return true;
  }
  event.preventDefault();
  dispatch(push(href));
};

export function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
}
export function isNotLeftClickEvent(event) {
  return event.button !== 0;
}
export function isPrevented(event) {
  return event.defaultPrevented === true;
}
export function isNotValid(a, href) {
  return (
    href.indexOf('#') === 0 ||
    href.indexOf('javascript:') === 0 /*eslint no-script-url: 0*/ ||
    href.indexOf('mailto:') === 0 ||
    !a.origin
  );
}
export function getAnchorByParent(el) {
  if (!el) {
    return false;
  }
  while (el) {
    if (el.nodeName === 'A') {
      return el;
    }
    el = el.parentElement;
  }
  return false;
}

export function getAnchorByPath(path) {
  return path.find(a => a.nodeName === 'A');
}
export function isExternal(a, origin) {
  return a.origin !== origin;
}
