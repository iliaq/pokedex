import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import PokeList from '../components/PokeList';
import { AppBar } from '../components';
//import actions from '../actions';

class MyPokesContainer extends PureComponent<Props, {}> {
  render() {
    const { my } = this.props;
    return (
      <Fragment>
        <AppBar count={my.length} />
        <PokeList results={my} />
      </Fragment>
    );
  }
}

export default connect(({ my }) => ({ my }))(MyPokesContainer as any);

export interface Props {
  my: any[];
}
