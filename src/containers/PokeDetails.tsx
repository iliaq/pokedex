import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import { PokeDetails, AppBar } from '../components';
import { addToMyList, getPokeDetails, removeFromMyList } from '../actions';
import { CircularProgress } from 'material-ui/Progress';
class PokeDetailsContainer extends PureComponent<Props> {
  state = {
    isLoading: true,
  };
  componentDidMount() {
    const { details, getPokeDetails, match: { params: { id } } } = this.props;
    if (details[id] === undefined) {
      this.setState({ isLoading: true });
      getPokeDetails(id).then(() => this.setState({ isLoading: false }));
    } else {
      this.setState({ isLoading: false });
    }
  }
  render() {
    const { addToMyList, removeFromMyList, details, my, match: { params: { id } } } = this.props;
    const { isLoading } = this.state;
    return (
      <Fragment>
        <AppBar count={my.length} />
        {isLoading ? <CircularProgress /> : <PokeDetails {...details[id]} {...{ addToMyList, removeFromMyList, my }} />}
      </Fragment>
    );
  }
}

export default connect(
  ({ details, my }) => ({
    details,
    my,
  }),
  {
    getPokeDetails,
    addToMyList,
    removeFromMyList,
  }
)(PokeDetailsContainer as any);

interface Props {
  match: any;
  my: any[];
  details: any;
  addToMyList(): any;
  removeFromMyList(): any;
  getPokeDetails(id: number): any;
}
