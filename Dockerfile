FROM node:alpine
WORKDIR /
COPY package.json /
RUN npm i
ENV PATH $PATH:/node_modules/.bin
WORKDIR /src
CMD ["npm", "start"]
