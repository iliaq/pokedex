.PHONY: webpack-dev-server dist
.DEFAULT_GOAL: webpack-dev-server

dc = docker-compose
gr = gitlab-runner exec shell
port ?= 9999
image ?= registry.gitlab.com/iliaq/pokedex
dr ?= docker run --rm
# serve:
# 	$(dr) -w /src -p $(port):$(port) -v `pwd`:/src \
# 	$(image) http

webpack-dev-server:
	$(dr) -w /src -p $(port):$(port) -v `pwd`:/src \
	$(image) /node_modules/.bin/webpack-dev-server -d \
		--host 0.0.0.0 --port $(port) --hot --inline \
		--output-public-path http://0.0.0.0:$(port)/

dist:
	$(dr) \
		-w /src \
		-v `pwd`:/src:rw \
	$(image) /node_modules/.bin/webpack ./src \
		--optimize-minimize \
		-p --display errors-only \
		--config webpack.prod.js

rollup:
	$(dr) \
		-v `pwd`:/src \
		-e NODE_ENV=development \
	$(image) rollup -c
ts:
	$(dr) \
		-v `pwd`:/src \
		-e NODE_ENV=development \
	$(image) tsc -p .

vendor:
	$(dr) -v `pwd`/vendor:/src \
	$(image) rollup -c 

ci.localy:
	$(gr) app
	$(gr) unit

image:
	docker build . -t front